# enable emacs style key bindings
set -g mode-keys emacs
set -g status-keys emacs

# urxvt tab like window switching and renaming (-n: no prior escape seq)
bind -n S-down new-window
bind -n S-up command-prompt "rename-window '%%'"
bind -n S-left prev
bind -n S-right next
bind -n C-left swap-window -t -1
bind -n C-right swap-window -t +1

bind -n C-up command-prompt "rename-session '%%'"

# set the wm title to connected host and current windo name
set -g set-titles on
set -g set-titles-string "#W (#T) - tmux"

# use all the colors we can
set -g default-terminal "screen-256color"

# save 10000 lines in the scrollback buffer
set -g history-limit 10000

# use 1-based index for counting windows, sessions, etc.
set -g base-index 1

# trigger a bell in the current window for any attached sessions
set -g bell-action any

############################################################################
# mouse                                                                    #
############################################################################

# enable the mouse
set -g mouse on

# enable middle click paste
unbind-key MouseDown2Pane
bind-key -n MouseDown2Pane run "tmux set-buffer \"$(xclip -o -sel clipboard)\"; tmux paste-buffer"

# -------------------------------------------------------------

######################
### DESIGN CHANGES ###
######################

# loud or quiet?
set -g visual-activity off
set -g visual-silence off
setw -g monitor-activity off

#  modes
setw -g clock-mode-colour "#99cc99"
setw -g mode-style "fg=#a09f93,bg=#515151"

# panes
set -g pane-border-style 'fg=colour19 bg=colour0'
set -g pane-active-border-style 'bg=colour0 fg=colour9'

# statusbar
set -g status-position top
set -g status-justify left
set -g status-style "fg=colour7 bg=#393939 dim"
set -g status-left ''
set -g status-right '#[fg=colour15,bg=#393939] #S '
set -g status-right-length 50
set -g status-left-length 20

setw -g window-status-current-style 'fg=colour15 bg=colour0 bold'
setw -g window-status-current-format ' #[fg=colour15]#I:#[fg=colour3]#W#[fg=colour15]#F '

setw -g window-status-style 'fg=colour15 bg=#393939'
setw -g window-status-format ' #[fg=colour15]#I:#[fg=colour2]#W#[fg=colour15]#F '

setw -g window-status-bell-style 'fg=colour1 bg=colour7 bold'

# messages
set -g message-style 'fg=colour5 bg=colour7 bold'
